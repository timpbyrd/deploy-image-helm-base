# vim: set filetype=yaml:
# Default values for gitlab-chart on the Google GKE Marketplace
# This is a YAML-formatted file.

###################################################################################################
# Global Values
###################################################################################################
global:
  operator:
    enabled: false
  application:
    create: true
    enabled: true
    allowClusterRoles: false
    links:
      - description: "Cloud Native GitLab Chart Documentation"
        url: "https://docs.gitlab.com/charts"
      - description: "Cloud Native GitLab Chart Repository"
        url: "https://gitlab.com/charts/gitlab"
      - description: "GitLab on the Google Marketplace"
        url: "https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab"
  hosts:
    domain: example.com
    https: true
    externalIP:
    ssh: ~
  ingress:
    configureCertmanager: false
  initialRootPassword: {}
  psql:
    password: {}
  redis:
    password: {}
  gitaly:
    authToken: {}
    internal:
      names: ['default']
    external: []
  minio:
    enabled: true
    credentials: {}
  appConfig:
    enableUsagePing: true
    defaultCanCreateGroup: true
    usernameChangingEnabled: true
    issueClosingPattern:
    defaultTheme:
    defaultProjectsFeatures:
      issues: true
      mergeRequests: true
      wiki: true
      snippets: true
      builds: true
    webhookTimeout:
    gravatar:
      plainUrl:
      sslUrl:
    extra:
      googleAnalyticsId:
      piwikUrl:
      piwikSiteId:
    lfs:
      bucket: git-lfs
      connection: {}
    artifacts:
      bucket: gitlab-artifacts
      connection: {}
    uploads:
      bucket: gitlab-uploads
      connection: {}
    packages:
      bucket: gitlab-packages
      connection: {}
    backups:
      bucket: gitlab-backups
      tmpBucket: tmp
    incomingEmail:
      enabled: false
      address: ""
      host: "imap.gmail.com"
      port: 993
      ssl: true
      startTls: false
      user: ""
      password:
        secret: ""
        key: password
      mailbox: inbox
      idleTimeout: 60
    ldap:
      servers: {}
    omniauth:
      enabled: false
      autoSignInWithProvider:
      syncProfileFromProvider: []
      syncProfileAttributes: ['email']
      allowSingleSignOn: ['saml']
      blockAutoCreatedUsers: true
      autoLinkLdapUser: false
      autoLinkSamlUser: false
      externalProviders: []
      providers: []
    pseudonymizer:
      configMap:
      bucket: gitlab-pseudo
      connection: {}
  shell:
    authToken: {}
    hostKeys: {}
  railsSecrets: {}
  registry:
    bucket: registry
    certificate: {}
    httpSecret: {}
  runner:
    registrationToken: {}
  # Outgoing email server settings
  smtp:
    enabled: false
    address: smtp.mailgun.org
    port: 2525
    user_name: ""
    password:
      secret: ""
      key: password
    # domain:
    authentication: "plain"
    starttls_auto: false
    openssl_verify_mode: "peer"
  # Email persona used in email sent by GitLab
  email:
    from: ''
    display_name: GitLab
    reply_to: ''
    subject_suffix: ''
  time_zone: UTC
  service:
    annotations: {}
  antiAffinity: soft
  workhorse: {}
  # configuration of certificates container & custom CA injection
  certificates:
    image:
      repository: registry.gitlab.com/gitlab-org/build/cng/alpine-certificates
      tag: 20171114-r3
    customCAs: []

###################################################################################################
# These must live outside the gitlab context because of issues with helm that prevent wrapper charts
# from understanding the parent-child relationship of charts in requirements.yaml
###################################################################################################
certmanager:
# requires prep such as DNS, etc for LetsEncrypt that won't be feasible for the marketplace at this time
  install: false
prometheus:
# turned off due to limited resources in the marketplace
  install: false
gitlab-runner:
# turned off to avoid hyper-scaling in the limited resources of marketplace
  install: false

###################################################################################################
# Values to send down into the wrapped charts
###################################################################################################
gitlab:
  # This entry controls when builiding from master
  task-runner:
    enabled: false
  nginx-ingress:
    enabled: true
    tcpExternalConfig: "true"
    controller:
      config:
        hsts-include-subdomains: "false"
        server-name-hash-bucket-size: "256"
        enable-vts-status: "true"
        use-http2: "false"
        ssl-ciphers: "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4"
        ssl-protocols: "TLSv1.1 TLSv1.2"
        server-tokens: "false"
      extraArgs:
        force-namespace-isolation: ""
      service:
        externalTrafficPolicy: "Local"
        kind: DaemonSet
      resources:
        requests:
          cpu: 50m
          memory: 100Mi
      publishService:
        enabled: true
      replicaCount: 1
      minAvailable: 0
      scope:
        enabled: true
      stats:
        enabled: true
      metrics:
        enabled: true
        service:
          annotations:
            prometheus.io/scrape: "true"
            prometheus.io/port: "10254"
    defaultBackend:
      replicaCount: 1
      minAvailable: 0
      resources:
        requests:
          cpu: 5m
          memory: 5Mi
    rbac:
      create: true
    serviceAccount:
      create: false
  redis-ha:
    nameOverride: redis
    enabled: false
  postgresql:
    install: true
    postgresUser: gitlab
    postgresDatabase: gitlabhq_production
    imageTag: 9.6.8
    usePasswordFile: true
    existingSecret: 'secret'
    metrics:
      enabled: true
  registry:
    minReplicas: 1
  shared-secrets:
    enabled: true
    rbac:
      create: true
    serviceAccount:
      create: false
  gitlab:
    unicorn:
      helmTests:
        enabled: false
      minReplicas: 1
      resources:
        limits:
         memory: 1.5G
        requests:
          cpu: 100m
          memory: 900M
      workhorse:
        resources:
          limits:
            memory: 100M
          requests:
            cpu: 10m
            memory: 10M
    sidekiq:
      minReplicas: 1
      resources:
        limits:
          memory: 1.5G
        requests:
          cpu: 100m
          memory: 500Mi
    gitlab-shell:
      minReplicas: 1
    # this entry controls when building from a helm version
    task-runner:
      enabled: false
  minio:
    resources:
      requests:
        memory: 64Mi
        cpu: 10m
  redis:
    resources:
      requests:
        cpu: 10m
        memory: 64Mi
