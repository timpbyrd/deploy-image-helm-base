#!/usr/bin/env bash
# vim: set filetype=bash:

# Defines shared functions and environment variables.
# This file should only be sourced, never directly invoked.

# verifies that the user has a defined registry to upload built deployer
# images
verify_registry_defined() {
    if [ -z "${GL_MP_REGISTRY}" ]; then
        display_failure "GL_MP_REGISTRY is not defined in the environment"
    fi

    if [ -z "${GL_MP_APP_NAME}" ]; then
        display_failure "GL_MP_APP_NAME is not defined in the environment"
    fi
}

# place a text string centered in a field of the specified length
# usage: center FIELD_WIDTH TEXT_STRING
center() {
    field_width=$1
    txt=$2

    awk -v fw="${field_width}" -v t="${txt}" \
        'BEGIN {
             ws=fw-length(t)
             rpad=int(ws/2)
             lpad=ws-rpad
             for(i=1; i <= lpad; i++)
                 printf FS
             printf t
             for(i=1; i <= rpad; i++)
                 printf FS
         }'
}

# display progress messages to stderr so that other scripts which may rely
# on reading from stdout are not impacted
display_output() {
    src=$(center 50 "$(basename "$0")")
    msg=$1
    exit_type=$2

    if [ -n "${msg}" ]; then
        echo "[${src}] ${msg}" >&2
    fi

    if [ "${exit_type}" = "failure" ]; then
      # run the cleanup before exit
      post_cleanup
      exit 1
    fi
}

display_task() {
  display_output "$1" "success"
}

display_failure() {
  msg="        $1"

  display_output "$1" "failure"
}

display_success() {
  msg="        $1"

  display_output "${msg}" "success"
}

display_warning() {
  msg="        $1"

  display_output "${msg}" "warning"
}

################################################################################
# Constants
################################################################################
REPOSITORY_BUILD="BUILD_FROM_REPOSITORY"
CHART_BUILD="BUILD_FROM_CHART"

################################################################################
# Directory Paths
################################################################################
repo_path="$(git rev-parse --show-toplevel)"
export GL_MP_REPO="${repo_path}"

export GL_MP_SCRIPTS="${GL_MP_REPO}/build-scripts"
export GL_MP_SCRIPT_LIBS="${GL_MP_SCRIPTS}/lib"
export GL_MP_CHART="${GL_MP_REPO}/container/chart/gitlab-mp"

export GL_MP_SCRATCH="${GL_MP_REPO}/.scratch"
export GL_MP_CONTAINER_CONTEXT="${GL_MP_REPO}/container"
export GL_MP_TEMPLATES="${GL_MP_REPO}/templates"
export GL_MP_CONF="${GL_MP_REPO}/conf"
export GL_MP_UPSTREAM_CHART_REPO="${GL_MP_SCRATCH}/gitlab"
export GL_MP_HELM_PREPARED="${GL_MP_SCRATCH}/.chart_source_ready"

export GL_MP_UPSTREAM_CHART_URL="https://gitlab.com/charts/gitlab.git"

################################################################################
# Config Files
################################################################################
export GL_MP_USER_ENV="${GL_MP_CONF}/user_env"
export GL_MP_VALUES_YAML="${GL_MP_CHART}/values.yaml"

################################################################################
# Template Files
################################################################################
export GL_MP_SCHEMA_TEMPLATE="${GL_MP_TEMPLATES}/schema.yaml.template"
export GL_MP_REQUIREMENTS_TEMPLATE="${GL_MP_TEMPLATES}/requirements.yaml.template"
export GL_MP_SA_MAPPING_YAML_TEMPLATE="${GL_MP_TEMPLATES}/mapping.yaml.template"

################################################################################
# Script Paths
################################################################################
export GL_MP_UPDATE_REQUIREMENTS="${GL_MP_SCRIPT_LIBS}/update-requirements-yml.sh"
export GL_MP_UPDATE_SCHEMA="${GL_MP_SCRIPT_LIBS}/update-schema-yml.sh"
export GL_MP_UPDATE_SA_MAPPING="${GL_MP_SCRIPT_LIBS}/update-mapping-yml.sh"
export GL_MP_RBAC_EXPORTER="${GL_MP_SCRIPTS}/rbac-exporter/convert_chart_roles_to_schema.rb"
export GL_MP_LIST_IMAGES="${GL_MP_SCRIPT_LIBS}/list-helm-images.sh"
export GL_MP_CHECKOUT_GL_UPSTREAM="${GL_MP_SCRIPT_LIBS}/checkout-upstream-branch.sh"
export GL_MP_PREPARE_HELM_SOURCE="${GL_MP_SCRIPT_LIBS}/prepare-helm-source.sh"
export GL_MP_BUILD_TARBALLS="${GL_MP_SCRIPT_LIBS}/build-gitlab-marketplace-chart-tarballs.sh"

################################################################################
# Files Created During Build Process
################################################################################
export GL_MP_SCHEMA_FILE="${GL_MP_CONTAINER_CONTEXT}/schema.yaml"
export GL_MP_REQUIREMENTS_FILE="${GL_MP_CHART}/requirements.yaml"
export GL_MP_REQUIREMENTS_LOCK_FILE="${GL_MP_CHART}/requirements.lock"
export GL_MP_SA_MAPPING_YAML="${GL_MP_CONF}/mapping.yaml"
export GL_MP_HELM_TARBALL_PATH="${GL_MP_CHART}/charts/"
export GL_MP_DEAD_FILES="${GL_MP_SCRATCH}/.dead_files"

################################################################################
# Read User Environment Data
################################################################################
if [ -f "${GL_MP_USER_ENV}" ]; then
    # shellcheck source=/dev/null
    . "${GL_MP_USER_ENV}"
fi

################################################################################
# Configure the Environment
################################################################################
# enable debugging if configured
if [ "${GL_MP_DEBUG}" = "yes" ]; then
    set -x
fi

# determine what kind of build is about to happen
if [ -z "${GL_MP_BUILD_TYPE}" ]; then
    if [ -n "${GITLAB_BRANCH_NAME}" ]; then
        GL_MP_BUILD_TYPE="${REPOSITORY_BUILD}"
        build_message="Using Cloud Native Gitlab branch ${GITLAB_BRANCH_NAME}"
    elif [ -n "${GL_CHART_VERSION}" ]; then
        GL_MP_BUILD_TYPE="${CHART_BUILD}"
        build_message="Using Cloud Native GitLab v${GL_CHART_VERSION}"
    else
        GL_MP_BUILD_TYPE="${REPOSITORY_BUILD}"
        GITLAB_BRANCH_NAME="master"
        build_message="Using Cloud Native GitLab master branch"
        export GITLAB_BRANCH_NAME
    fi
    display_task "${build_message}"
fi
export GL_MP_BUILD_TYPE

# Define this if not already defined in the environment
if [ -z "${GCR_REGISTRY}" ]; then
    verify_registry_defined
    export GCR_REGISTRY="${GL_MP_REGISTRY}/${GL_MP_APP_NAME}"
fi

# Define this if not already defined in the environment
if [ -z "${DEPLOYER_TAG}" ]; then
    branch_name=$(git rev-parse --abbrev-ref HEAD)

    # Use the commit SHA as the tag if no branch name found
    if [ "${branch_name}" = "HEAD" ]; then
        branch_name=$(git rev-prase HEAD)
    fi

    [ -z "${branch_name}" ] && display_failure "Could not figure out the tag"

    export DEPLOYER_TAG="${branch_name}"
fi

if [ -n "${CI_REGISTRY:-}" ]; then
    export BUILD_REGISTRY="${CI_REGISTRY}"
fi

# CI Environment needs this set
if [ -n "${CI}" ] && [ -z "${GL_MP_APP_INSTANCE_NAME}" ]; then
    GL_MP_APP_INSTANCE_NAME="gl-mp-ci"
fi

# Create everything required for a build in case it's missing
[ ! -d "${GL_MP_SCRATCH}" ] && mkdir -p "${GL_MP_SCRATCH}"
[ ! -d "${GL_MP_HELM_TARBALL_PATH}" ] && mkdir -p "${GL_MP_HELM_TARBALL_PATH}"

################################################################################
# Function Definitions
################################################################################
remove_file() {
    file_name="$1"

    if [ -n "${file_name}" ]; then
        if [ -f "${file_name}" ]; then
            if rm "${file_name}"; then
                display_success "Removed ${file_name}"
            else
                display_failure "Could not remove ${file_name}"
            fi
        fi
    fi
}

add_dead_file() {
    dead_file="$1"

    if [ -n "${dead_file}" ]; then
        echo "${dead_file}" >> "${GL_MP_DEAD_FILES}"
    fi
}

# remove files that should not be cached between builds
post_cleanup() {
    if [ -f "${GL_MP_DEAD_FILES}" ]; then
      while IFS= read -r file_name
      do
        remove_file "${file_name}"
      done < "${GL_MP_DEAD_FILES}"

      remove_file "${GL_MP_DEAD_FILES}"
    fi

    remove_file "${GL_MP_HELM_PREPARED}"
}

get_gitlab_helm_chart_path() {
    if [ "${GL_MP_BUILD_TYPE}" = "${REPOSITORY_BUILD}" ]; then
        echo "${GL_MP_UPSTREAM_CHART_REPO}"
    else
        echo "${GL_MP_CHART}"
    fi
}

get_gitlab_versions() {
    version_number=$(helm template "${GL_MP_CHART}" | yq -r ". | select( .kind == \"Application\" )| .spec.descriptor.version")
    check_number=$(echo "${version_number}" |sed -n 's/^\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)$/\1.\2.\3/p')

    if [ "${version_number}" = "${check_number}" ]; then
        major_minor=$(echo "${version_number}" |sed -n 's/^\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)$/\1.\2/p')
        echo "${version_number} ${major_minor}"
    else
        display_failure "${version_number} is not a semantic version"
    fi
}

get_container_tags() {
    registries=()
    if [ -z "$1" ]; then
        registries+=("deployer")
    fi

    if [ -n "${GL_REGISTRY_TAG}" ]; then
        registries+=("${GL_REGISTRY_TAG}/deployer")
    fi

    registries+=("${GCR_REGISTRY}/deployer")

    container_tags=()

    if [ "${GL_MP_BUILD_TYPE}" = "${REPOSITORY_BUILD}" ]; then
        tags=("${GITLAB_BRANCH_NAME}")
    else
        tags=()
        for version in $(get_gitlab_versions); do
            tags+=("${version}")
        done
    fi

    for tag in "${tags[@]}"; do
        for registry in "${registries[@]}"; do
            display_success "adding ${version} ${registry}"
            container_tags+=("${registry}:${tag}")
        done
    done
    echo "${container_tags[@]}"
}
