# KubernetesSubject
#
# Class describing a subject within a Kubernetes
# helm chart.
#
# NOTE: This class only supports ServiceAccounts for the moment

require_relative 'kubernetes_role'

class KubernetesSubject
  # instance variables
  attr_accessor :subject_kind
  attr_accessor :subject_name
  attr_accessor :subject_namespace
  attr_accessor :roles
  attr_accessor :subject_name_property

  def initialize(subject = nil)
    raise ArgumentError, 'Subject parameter was nil' if subject.nil?

    @subject_kind = subject["kind"]
    @subject_name = subject["metadata"]["name"]
    @subject_namespace = subject["metadata"]["namespace"]
    @subject_name_property = nil

    # Roles are added after the initialization
    @roles = []
  end

  def set_name_property(mappings = nil)
    return if mappings.nil?

    raise ArgumentError, 'set_name_property expects a Hash' unless mappings.is_a? Hash

    return unless mappings.key? subject_name

    self.subject_name_property = mappings[subject_name]
  end

  def define_roles(roles = nil, role_bindings = nil)
    # add roles bound to this subject in the rolebindings
    role_bindings.each do |rb|
      current_role = KubernetesRole.new rb
      rb["subjects"].each do |binding_subject|
        next unless subject_kind == binding_subject["kind"] &&
          subject_name == binding_subject["name"] &&
          subject_namespace == binding_subject["namespace"]

        self.roles.push(current_role) unless self.roles.include? current_role
      end
    end

    # define the rules for the roles bound to this subject
    roles.each do |role|
      role_index = self.roles.index(KubernetesRole.new(role))
      self.roles[role_index].define_rules role unless role_index.nil?
    end
  end

  def ==(other)
    subject_kind == other.subject_kind &&
      subject_name == other.subject_name &&
      subject_namespace == other.subject_namespace
  end

  def to_s
    <<~EOS
      Subject Kind:      #{@subject_kind}
      Subject Name:      #{@subject_name}
      Subject Namespace: #{@subject_namespace}
      #{@roles}
    EOS
  end

  # GKE Marketplace only supports ServiceAccount
  def structure
    # GKE Marketplace only talks about ServiceAccounts for now
    raise NotImplementedError, 'GKE Marketplace only handles Service Account Types for now' unless @subject_kind == "ServiceAccount"

    s = {}

    subject_name = @subject_name_property.nil? ? @subject_name : @subject_name_property

    s[subject_name] = {}
    s[subject_name]["type"] = "string"
    s[subject_name]["x-google-marketplace"] = {}
    s[subject_name]["x-google-marketplace"]["type"] = "SERVICE_ACCOUNT"
    s[subject_name]["x-google-marketplace"]["service_account"] = {}
    s[subject_name]["x-google-marketplace"]["service_account"]["roles"] = []

    roles.each do |r|
      s[subject_name]["x-google-marketplace"]["service_account"]["roles"].push(r.config)
    end

    s
  end
end
