require 'spec_helper'

describe "Chart Objects" do
  context "when initialized with no parameter" do
    it "should have zero objects" do
      expect(ChartObjects.new.size).to eq(0)
    end
  end

  context "when initialized from a file" do
    chart_fixture = ChartObjects.new File.expand_path("spec/fixtures/chart.yaml")
    it "should have more than zero objects" do
      expect(chart_fixture.size).to be > 0
    end
  end

  context "when initialized from a file with accounts" do
    chart_fixture = ChartObjects.new File.expand_path("spec/fixtures/chart.yaml")

    it "should have more than zero rbac entries" do
      expect(chart_fixture.rbac_entries.length).to be > 0
    end

    it "should have more than zero subjects" do
      expect(chart_fixture.kubernetes_subjects.length).to be > 0
    end

    it "should generate expected structure" do
      expected_structure = YAML.load_stream(File.read(File.expand_path("spec/fixtures/schema_output_from_chart.yaml")))
      received_structure = []
      chart_fixture.kubernetes_subjects.each do |sub|
        received_structure.push(sub.structure)
      end
      expect(expected_structure).to eq(received_structure)
    end
  end

  context "when given a subject mapping yaml" do
    fa = File.expand_path("spec/fixtures/chart.yaml")
    fb = File.expand_path("spec/fixtures/service_account_mappings.yaml")
    fc = File.expand_path("spec/fixtures/service_account_mappings_mismatch.yaml")
    fd = File.expand_path("spec/fixtures/service_account_mappings_miscount.yaml")

    it "should raise an Argument Error if given a non-existent mapping file" do
      expect { ChartObjects.new fa, "idontexist.txt" }.to raise_error(ArgumentError)
    end
    it "should have a 1:1 mapping of subjects to remap targets" do
      chart_fixture = ChartObjects.new fa, fb
      expect(chart_fixture.subjects_match_remaps?).to be true
    end

    it "should raise an exception when subject names don't match remaps" do
      expect { ChartObjects.new fa, fc }.to raise_error(ArgumentError)
    end

    it "should raise an exception when count of subject names don't equal remaps" do
      expect { ChartObjects.new fa, fd }.to raise_error(ArgumentError)
    end

    it "should generate expected structure" do
      chart_fixture = ChartObjects.new fa, fb
      expected_structure = YAML.load_stream(File.read(File.expand_path("spec/fixtures/schema_output_from_chart_remapped.yaml")))
      received_structure = []
      chart_fixture.kubernetes_subjects.each do |sub|
        received_structure.push(sub.structure)
      end
      expect(expected_structure).to eq(received_structure)
    end
  end
end
