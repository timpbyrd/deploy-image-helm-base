#!/usr/bin/env bash

set -eo pipefail

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

"${GL_MP_PREPARE_HELM_SOURCE}" || display_failure "Could not prepare helm source"

chart_path=$(get_gitlab_helm_chart_path)

[ ! -d "${chart_path}" ] && display_failure "Cannot find ${chart_path}"

pushd "${chart_path}" > /dev/null

display_task "Generating list of container images"
if helm template . --set certmanager-issuer.email=none@none.com -f "${GL_MP_VALUES_YAML}" | \
        yq -r ". | select( .kind == \"Job\" or .kind == \"Deployment\" or .kind == \"StatefulSet\" or .kind == \"DaemonSet\" ) | .spec.template.spec | [.containers,.initContainers] | .[] | select(.!=null) | .[].image" | \
   sort | uniq; then
    display_success "complete"
else
    display_failure "failed"
fi

popd > /dev/null
