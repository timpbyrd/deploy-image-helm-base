# Deploying GitLab to GKE via Google Cloud Marketplace

## Overview

[GitLab](https://about.gitlab.com) is a single application for the complete DevOps lifecycle from project planning and source code management to CI/CD and monitoring.

The [Google Cloud Marketplace](https://cloud.google.com/launcher/) is a easy way to deploy apps like GitLab to a [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) cluster, with just a few clicks.

> **Note:** For production deployments, we recommend using the [`gitlab` Helm chart](https://docs.gitlab.com/ee/install/kubernetes/gitlab_chart.html) and configuring [external PostgreSQL, Redis, and object storage services](https://gitlab.com/charts/gitlab/tree/master/doc/advanced).

## Installation

### Quick install with Google Cloud Marketplace

Deploy GitLab to Google Kubernetes Engine using Google Cloud Marketplace, by following the [on-screen instructions](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab).

### Command line instructions

-  The same tools used for creating the GKE Marketplace image are required to deploy manually into a Kubernete cluster. [Follow the build dependency instructions noting that ruby and git are not required for deployment](doc/building-deployer.md#build-dependencies).
- If a cluster is not already created and configured within `kubectl`, [follow the directions for creating a cluster and creating the connection](doc/building-deployer.md#create-a-google-kubernetes-engine-cluster).
- [Configure Docker authentication](doc/building-deployer.md#configuring-authentication).

Assuming everything has gone correctly with the installation the next step
is to install the deployer. Note that `conf/user_env` may be set rather than
exporting the variables as noted below and is sourced by the tooling.

| Value | Description|
| --- | --- |
| **TAG** | The deployer container tag describes the GitLab version that will be released. |
| **GL\_MP\_APP\_INSTANCE\_NAME** | The application name defined in Kubernetes. |
| **GL\_MP\_NAMESPACE** | The application will be installed to this namespace in Kubernetes. |
| **GL\_MP\_TLD** | The top level domain for the application. ***example.com*** will result in an application available at ***gitlab.example.com***. |
| **GL\_MP\_LOADBALANCER\_IP** | The IP Address of the external loadbalancer if deploying via GKE On Premise. |
| **GCR\_REGISTRY** | The assembled path to the deployer container and the tag identifying the desired version. |
| **GL\_MP\_PARAMS** | The parameters that will be passed to the deployer when installing the GitLab application. |

```sh
export TAG="latest"
export GL_MP_APP_INSTANCE_NAME="gl-mp-app"
export GL_MP_NAMESPACE="gl-app-namespace"
export GL_MP_TLD="example.com"
export GCR_REGISTRY="https://console.cloud.google.com/gcr/images/cloud-marketplace/GLOBAL/gitlab-public/gitlab/deployer:${TAG}"
export GL_MP_PARAMS="{\"APP_INSTANCE_NAME\": \"${GL_MP_APP_INSTANCE_NAME}\",\"NAMESPACE\": \"${GL_MP_NAMESPACE}\", \"global.hosts.domain\": \"${GL_MP_TLD}\"}"
# Optional: Create this secret to install valid TLS certificates for the top level domain selected
kubectl create secret tls -n mkt-ns RELEASE-NAME-wildcard-tls --cert=CHAINED_CERT_FILE --key=KEY_FILE

# Runs the installation
mpdev /scripts/install --deployer="${GCR_REGISTRY}" --parameters="${GL_MP_PARAMS}"
```

> **DANGER: Optional TLS Secret**
> The Cloud Native GitLab application will provide self sigend certificates if
> this is not specified. There are a number of implications to using
> self-signed certificates. [Consult the documentation about TLS for more
> information](https://docs.gitlab.com/charts/installation/tls.html).

## Pointing DNS at the GitLab Instance

Retrieve the IP address GitLab is available at, note it may take a few minutes for the IP address to populate:

```sh
kubectl get \
    --namespace NAMESPACE \
    ingress RELEASE-NAME-unicorn \
    -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
```

Configure a wildcard DNS record for the top level domain provided during installation, resolving to the IP address retrieved above.

## Signing in

Browse to https://`gitlab.<yourdomain>`.

Retrieve the password for the `root` user account with:

```shell
# specify the variables values matching your installation:
export RELEASE_NAME=RELEASE-NAME
export NAMESPACE=NAMESPACE

kubectl get secret -n $NAMESPACE $RELEASE_NAME-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode
```

# Administration of GitLab

GitLab offers a number of different options to customize the behavior to your needs. More information is available in our [administration documentation](https://docs.gitlab.com/charts/#updating-gitlab-using-the-helm-chart).

## Update GitLab

Updating GitLab via the GKE Marketplace is not yet supported. Consult with
the [main chart
documentation](https://docs.gitlab.com/ee/install/kubernetes/gitlab_chart.html)
to evaluate update options. Note that the default values are [configured in
the `values.yaml` defined in the Marketplace
chart](container/chart/gitlab-mp/values.yaml).
